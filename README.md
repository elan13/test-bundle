Installation
============

Works on Symfony ^3.4|^4.3

Download the Bundle
----------------------------------
1.Open composer.json file and add these lines

```
"repositories": [
        {
          "type": "vcs",
          "url": "git@bitbucket.org:elan13/gv24test-bundle.git"
        }
    ]
```

2.Open a command console, enter your project directory and execute:

```console
$ composer require gv/gv24test-bundle:*@dev
```


Make sure that Bundle enabled:
----------------------------------------

### Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

```php
// config/bundles.php

return [
    // ...
    GV24\Bundle\GV24TestBundle\GV24TestBundle::class => ['all' => true],
];
```



Make sure assets are installed
----------------------------------------

Bundle requires "public/bundles/gv24test/css/calculator-options.css" 
(that asset's taken directly from bundle. But you can override it).

If you don's see above file, than you need to run command
in order to install `calculator-options.css` from bundle

```console
$ php bin/console assets:install
```


Configurations
----------------------------------------

### Step 1: Import routes

Then, in order to use routes from the bundle
in the `config/routes.yaml` file of your project:

```php
// config/routes.yaml

gv24:
  resource: '@GV24TestBundle/Resources/config/routes.xml'
  prefix: /gv24
```

### Step 2: Migrate tables

```console
$ php bin/console doctrine:migrations:diff
```
Make sure all sql generated correctly.

Run migrations:

```console
$ php bin/console doctrine:migrations:migrate
```

### Step 3: Fill new tables with your custom data


### Step 4:  Page `/gv24/co` available for browsing


--------------------------------------