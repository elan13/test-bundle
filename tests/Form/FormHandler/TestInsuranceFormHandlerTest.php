<?php

namespace GV24\Bundle\GV24TestBundle\Tests\Form\FormHandler;

use Doctrine\ORM\EntityManagerInterface;
use GV24\Bundle\GV24TestBundle\Entity\TestCalculator;
use GV24\Bundle\GV24TestBundle\Entity\TestInsurer;
use GV24\Bundle\GV24TestBundle\Form\FormHandler\TestInsuranceFormHandler;
use GV24\Bundle\GV24TestBundle\Form\TestCollectionInsurersType;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Exception\LogicException;

/**
 * Tested on Symfony 4.3
 *
 * Class TestInsuranceFormHandlerTest
 *
 * @package GV24\Bundle\GV24TestBundle\Tests\Form\FormHandler
 */
class TestInsuranceFormHandlerTest extends KernelTestCase
{
    /**
     * @var EntityManagerInterface
     */
    private static $entityManager;

    /**
     * @var TestInsurer[]
     */
    private static $insurers;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        static::bootKernel();

        static::$entityManager = static::$container->get('doctrine.orm.default_entity_manager');
        static::$insurers =  static::$entityManager->getRepository(TestInsurer::class)->findAllWithCalculators();
    }

    public function testHandleSuccess()
    {
        // change isActive in order to make sure that Handler changes values
        static::$insurers[0]->getCalculators()[0]->setIsActive(false);
        static::$insurers[1]->getCalculators()[0]->setIsActive(true);
        static::$insurers[1]->getCalculators()[1]->setIsActive(false);


        // omit csrf protection in order to have a successful handle
        $form = static::$container->get('form.factory')->create(TestCollectionInsurersType::class, [
            'insurers' => static::$insurers,
        ], ['csrf_protection' => false]);


        $request = new Request();
        $request->request->set('test_collection_insurers', [
            'insurers' => [
                ['calculators' => [['isActive' => true]]],
                [
                    'calculators' => [
                        ['isActive' => false],
                        ['isActive' => true],
                    ]
                ],
            ],
        ]);

        $form->submit($request->request->get($form->getName()));
        $formHandler = new TestInsuranceFormHandler($request, static::$entityManager);

        $this->assertTrue($formHandler->handle($form));
        $this->assertTrue(static::$insurers[0]->getCalculators()[0]->getIsActive());
        $this->assertFalse(static::$insurers[1]->getCalculators()[0]->getIsActive());
        $this->assertTrue(static::$insurers[1]->getCalculators()[1]->getIsActive());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testHandleDoesNotAllowFormTypeExceptTestCollectionInsurers()
    {
        // create builder in order to have FormType name not test_collection_insurers
        $form = static::$container->get('form.factory')->createBuilder(FormType::class)->getForm();
        $request = new Request();

        $formHandler = new TestInsuranceFormHandler($request, static::$entityManager);
        $formHandler->handle($form);
    }

    /**
     * @expectedException LogicException
     */
    public function testFormDoesNotAllowTypesExceptTestInsurer()
    {
        // pass TestCalculator, to make sure it doesn't allow any types except TestInsurer
        $form = static::$container->get('form.factory')->create(TestCollectionInsurersType::class, [
            'insurers' => [new TestCalculator()],
        ], ['csrf_protection' => false]);

        $request = new Request();
        $request->request->set('test_collection_insurers', [
            'insurers' => [
                ['calculators' => [['isActive' => true]]],
            ],
        ]);

        $formHandler = new TestInsuranceFormHandler($request, static::$entityManager);
        $formHandler->handle($form);
    }

    public function testHandleExtraFieldsError()
    {
        static::$insurers[0]->getCalculators()[0]->setIsActive(false);

        // omit csrf protection in order to have a successful handle
        $form = static::$container->get('form.factory')->create(TestCollectionInsurersType::class, [
            'insurers' => static::$insurers,
        ], ['csrf_protection' => false]);


        $request = new Request();

        // add request data, but with extra keys
        $request->request->set('test_collection_insurers', [
            'insurers' => [
                [
                    'calculators' => [
                        ['isActive' => false, 'extra_field' => 'extra_field_value'],
                        ['extra_field' => 'extra_field_value']
                    ]
                ],
            ],
        ]);

        $form->submit($request->request->get($form->getName()));
        $formHandler = new TestInsuranceFormHandler($request, static::$entityManager);

        $this->assertFalse($formHandler->handle($form));
        $this->assertFalse(static::$insurers[0]->getCalculators()[0]->getIsActive());
        $this->assertCount(2, $form->getErrors());
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        static::ensureKernelShutdown();
    }
}