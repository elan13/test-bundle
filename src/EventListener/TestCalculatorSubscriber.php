<?php

namespace GV24\Bundle\GV24TestBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use GV24\Bundle\GV24TestBundle\Entity\LogTestCalculatorActivity;
use GV24\Bundle\GV24TestBundle\Entity\TestCalculator;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ServiceSubscriberInterface;

class TestCalculatorSubscriber implements EventSubscriber, ServiceSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * we need container for Lazy Loading, or all our dependencies would be loaded every doctrine event
     *
     * TestCalculatorSubscriber constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }


    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return string[]
     */
    public function getSubscribedEvents()
    {
        return [
            Events::postUpdate,
        ];
    }

    /**
     * Log changes after actual persisting
     *
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        // skip this logic if entity is NOT TestCalculator
        if (!$entity instanceof TestCalculator) {
            return;
        }

        // logs must NOT thow exception
        try {
            /** @var EntityManagerInterface $entityManager */
            $entityManager = $this->container->get(EntityManagerInterface::class);

            $log = new LogTestCalculatorActivity();
            $log->setCalculator($entity);
            $log->setIsActive($entity->getIsActive());

            $entityManager->persist($log);
            $entityManager->flush();
        } catch (\Throwable $e) {
            /** @var  LoggerInterface $logger */
            $logger = $this->container->get(LoggerInterface::class);

            $logger->notice($e->getMessage());
        }
    }

    /**
     * @return array The required service types, optionally keyed by service names
     */
    public static function getSubscribedServices()
    {
        return [
            LoggerInterface::class,
            EntityManagerInterface::class,
        ];
    }
}