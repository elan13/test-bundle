<?php

namespace GV24\Bundle\GV24TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * GV24\Bundle\GV24TestBundle\Entity\TestCalculator
 *
 * @ORM\Table(name="test_calculators")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="GV24\Bundle\GV24TestBundle\Repository\TestCalculatorRepository")
 */
class TestCalculator
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean")
     * @Assert\Type("bool")
     */
    private $isActive;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=250)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=250)
     * @Assert\NotBlank()
     */
    private $tag;

    /**
     * @var TestInsurer
     *
     * @ORM\ManyToOne(targetEntity="TestInsurer", inversedBy="calculators")
     */
    private $insurer;

    public function __construct()
    {
        $this->isActive = true;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $calcName
     * @return TestCalculator
     */
    public function setName($calcName)
    {
        $this->name = $calcName;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * @return TestCalculator
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set insurer
     *
     * @param TestInsurer $insurer
     * @return TestCalculator
     */
    public function setInsurer($insurer)
    {
        $this->insurer = $insurer;
    
        return $this;
    }

    /**
     * Get insurer
     *
     * @return TestInsurer
     */
    public function getInsurer()
    {
        return $this->insurer;
    }

    /**
     * Set isActive
     *
     * @param bool $isActive
     * @return TestCalculator
     */
    public function setIsActive(bool $isActive = true)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return bool
     */
    public function getIsActive()
    {
        return (bool) $this->isActive;
    }
}
