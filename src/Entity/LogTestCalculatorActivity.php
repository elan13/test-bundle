<?php

namespace GV24\Bundle\GV24TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="log_test_calculators_activity")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="GV24\Bundle\GV24TestBundle\Repository\LogTestCalculatorRepository")
 */
class LogTestCalculatorActivity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TestCalculator")
     * @ORM\JoinColumn(nullable=false)
     */
    private $calculator;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="datetime")
     */
    private $changedAt;


    public function __construct()
    {
        $this->changedAt = new \DateTime();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCalculator(): ?TestCalculator
    {
        return $this->calculator;
    }

    public function setCalculator(?TestCalculator $calculator): self
    {
        $this->calculator = $calculator;

        return $this;
    }

    public function getChangedAt(): ?\DateTimeInterface
    {
        return $this->changedAt;
    }

    public function setChangedAt(\DateTimeInterface $changedAt): self
    {
        $this->changedAt = $changedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
