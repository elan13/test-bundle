<?php

namespace GV24\Bundle\GV24TestBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * GV24\Bundle\GV24TestBundle\Entity\TestInsurer
 *
 * @ORM\Table(name="test_insurers")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="GV24\Bundle\GV24TestBundle\Repository\TestInsurerRepository")
 */
class TestInsurer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=250)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var TestCalculator[]
     *
     * @ORM\OneToMany(targetEntity="TestCalculator", mappedBy="insurer")
     */
    private $calculators;



    public function __construct()
    {
        $this->calculators = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $calcName
     * @return TestInsurer
     */
    public function setName($calcName)
    {
        $this->name = $calcName;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return TestCalculator[]|Collection
     */
    public function getCalculators(): Collection
    {
        return $this->calculators;
    }

    /**
     * @param TestCalculator $calculator
     * @return TestInsurer
     */
    public function addCalculator(TestCalculator $calculator): self
    {
        if (!$this->calculators->contains($calculator)) {
            $this->calculators[] = $calculator;
            $calculator->setInsurer($this);
        }
        return $this;
    }

    /**
     * @param TestCalculator $calculator
     * @return TestInsurer
     */
    public function removeProduct(TestCalculator $calculator): self
    {
        if ($this->calculators->contains($calculator)) {
            $this->calculators->removeElement($calculator);

            // make sure that owning side is null
            if ($calculator->getInsurer() === $this) {
                $calculator->setInsurer(null);
            }
        }

        return $this;
    }
}
