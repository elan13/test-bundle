<?php

namespace GV24\Bundle\GV24TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TestCollectionInsurersType
 *
 * @package GV24\Bundle\GV24TestBundle\Form
 */
class TestCollectionInsurersType extends AbstractType
{
    /**
     * form name, that used as a prefix and identifier of the form.
     * We must use it in case it's gonna change and ruin depended services
     */
    const FORM_NAME = 'test_collection_insurers';


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('insurers', CollectionType::class, [
                'entry_type' => EmbededTestInsurerType::class,
            ])

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'gv24_test',
        ]);
    }


    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return self::FORM_NAME;
    }
}
