<?php

namespace GV24\Bundle\GV24TestBundle\Form;

use GV24\Bundle\GV24TestBundle\Entity\TestInsurer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EmbededTestInsurerType
 *
 * @package GV24\Bundle\GV24TestBundle\Form
 */
class EmbededTestInsurerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('calculators', CollectionType::class, [
                'entry_type' => EmbededTestCalculatorType::class,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TestInsurer::class
        ]);
    }
}
