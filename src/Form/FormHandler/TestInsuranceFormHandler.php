<?php

namespace GV24\Bundle\GV24TestBundle\Form\FormHandler;

use Doctrine\ORM\EntityManager;
use GV24\Bundle\GV24TestBundle\Form\TestCollectionInsurersType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TestInsuranceFormHandler
 *
 * @package GV24\Bundle\GV24TestBundle\Form\FormHandler
 */
class TestInsuranceFormHandler implements FormHandlerInterface
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * TestInsuranceFormHandler constructor.
     * @param Request $request
     * @param EntityManager $entityManager
     */
    public function __construct(Request $request, EntityManager $entityManager)
    {
        $this->request = $request;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(FormInterface $form)
    {
        if (TestCollectionInsurersType::FORM_NAME !== $form->getName()) {
            throw new \InvalidArgumentException(sprintf('Expected form with name "%s", "%s" given', TestCollectionInsurersType::class, $form->getName()));
        }

        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();

            foreach ($formData['insurers'] as $insurer) {
                foreach ($insurer->getCalculators() as $calculator) {
                    $this->entityManager->persist($calculator);
                }
            }

            $this->entityManager->flush();

            return true;
        }

        return false;
    }
}