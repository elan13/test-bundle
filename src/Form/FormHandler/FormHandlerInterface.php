<?php

namespace GV24\Bundle\GV24TestBundle\Form\FormHandler;

use Symfony\Component\Form\FormInterface;

/**
 * Interface FormHandlerInterface
 *
 * @package GV24\Bundle\GV24TestBundle\Form\FormHandler
 */
interface FormHandlerInterface
{
    /**
     * @param FormInterface $form
     * @return mixed
     */
    public function handle(FormInterface $form);
}