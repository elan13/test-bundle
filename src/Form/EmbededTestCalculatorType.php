<?php

namespace GV24\Bundle\GV24TestBundle\Form;

use GV24\Bundle\GV24TestBundle\Entity\TestCalculator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EmbededTestCalculatorType
 *
 * @package GV24\Bundle\GV24TestBundle\Form
 */
class EmbededTestCalculatorType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isActive', CheckboxType::class, ['label' => false])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TestCalculator::class,
        ]);
    }
}
