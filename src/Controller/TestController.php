<?php

namespace GV24\Bundle\GV24TestBundle\Controller;

use Doctrine\ORM\EntityManager;
use GV24\Bundle\GV24TestBundle\Entity\TestInsurer;
use GV24\Bundle\GV24TestBundle\Form\FormHandler\TestInsuranceFormHandler;
use GV24\Bundle\GV24TestBundle\Form\TestCollectionInsurersType;
use GV24\Bundle\GV24TestBundle\Repository\TestInsurerRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * test controller.
 */
class TestController extends AbstractController
{
    /**
     * Calculator options action.
     */
    public function calculatorOptions(Request $request)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        /** @var TestInsurerRepository $insurerRepository */
        $insurerRepository = $entityManager->getRepository(TestInsurer::class);


        // create from, with TestInsurer[]
        $form = $this->createForm(TestCollectionInsurersType::class, [
            'insurers' => $insurerRepository->findAllWithCalculators(),
        ]);

        // handle form submit
        if ($request->isMethod('POST')) {
            // handle form data, and persist isActive changes for TestCalculator Entity
            $formHandler = new TestInsuranceFormHandler($request, $entityManager);

            if ($formHandler->handle($form)) {
                // redirect from POST request to GET
                return $this->redirect($request->getUri());
            }
        }

        return $this->render('@GV24Test\test\calculator_options.html.twig', [
                'form' => $form->createView(),
        ]);
    }
}